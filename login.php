
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/login.css" />
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    
    <script src="./js/jquery.js"></script>
    
</head>

    <body>
        <div class="container-fluid" >
            <div class="row">
            <div class="col-5"></div>
                <div class="onlylogin">
                    <div class="login-page">
                   
                        <div class="form" id="login1">
                            <div class="alert alert-danger" role="alert" id="displayerror" style="display:none">
                                Username or password incorrect!
                            </div>
                            <form  id="login" method="post" action="login.php" class="login-form">
                                <input class="rounded" type="text" id="username" name="name" placeholder="username" autocomplete="off"/>
                                <input class="rounded" type="password" id="password" name="password" placeholder="password"/>
                                <button class="btn btn-success btn-lg btn-block" onclick=clickLogin() >Login</button> 
                                <a class="btn btn-danger btn-lg btn-block mt-2" href="index.php" >Cancel</a>
                                <p class="message">Δεν έχετε εγγραφεί? <a href="register.php">Δημιουργήστε Λογαριασμό</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    </body>

    <script>
        function clickLogin(){
            request = $.ajax({
                url: "loginandregister.php",
                type: "post",
                data: {action: 'login',
                    username: document.getElementById('username').value,
                    password: document.getElementById('password').value}
            })
            request.done(function(response){

                console.log(response);
                if(response==1){

                    window.location.href = "http://localhost/sdi1500061_sdi1500117/index.php";
                }
                else{
                    document.getElementById('displayerror').style.display = "block";

                }
            })

            request.fail(function(jqXHR, textStatus, errorThrown){
            
                console.error("Error occured my friend");
            })
        }
    </script>
    
    <script>
    $("#login").submit(function(e) {
        e.preventDefault();
    });
</script>
</html>


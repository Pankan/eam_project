<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Eudoxus-Δήλωση Συγγραμάτων</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/bootstrap-grid.css" type="text/css">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row mt-1">
        <div class="col-4"></div>
        <div class="col-6 home-image">
        <img src="images/evdoksos.png" alt="placeholder" style="border:1px solid black;width:400px;height:150px;" class="rounded">  
        </div>
        <div class="col-2"></div>    
        </div>
        <div class="row mt-2">
        <div class="col-12">
        <nav class="navbar rounded sticky-top navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="index.php">Εύδοξος</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Αρχική <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Φοιτητές
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="profile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dilosi.php">Δήλωση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Ανταλλαγή Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="istorikodilwsewn.php">Ιστορικό Δηλώσεων</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Εκδότες
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="ekdotisprofile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="ekdothsBiblia.php">Διαχείρηση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Κοστολόγηση</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Γραμματεία
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Placeholder</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Placeholder</a>
                </div>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Νεα-Ανακοινώσεις</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="searchbooks.php">Αναζήτηση Βιβλίων</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="help.php">Βοήθεια</a>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Username"] ?>
                    
                  </a>
                  
                <?php else: ?>
                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Ekdoths"] ?>
                    </a>
                    
                  <?php else: ?>
                    <li class="nav-item"><a class="nav-link" href="./register.php"><i class="fas fa-user-plus mr-1"></i> Εγγραφή</a></li>

                  <?php endif ?>

                <?php endif ?>
                <li class="nav-item">
                <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="logout.php"> 
                    
                    <?php echo "Αποσύνδεση" ?>
                    
                  </a>
                <?php else: ?>

                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="logout.php"> 
                    
                      <?php echo "Αποσύνδεση" ?>
                    </a>
                  <?php else: ?>
                    <a class="nav-link" href="login.php"><i class="fas fa-sign-in-alt mr-1"></i> 
                    Σύνδεση
                    </a>
                  <?php endif ?>

                <?php endif ?>
                
              
                
              </li>
            </ul>  
          </div>
        </nav>
        </div>
        </div>
        <!--Breadcrunmb here-->
        <div class="row">
        <div class="col-md-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mt-0 mb-2 bg-light">
                <!--All previous pages here -->
                <li class="breadcrumb-item"><a href="index.php">Αρχική</a></li>
                <!--Current Page-->
                <li class="breadcrumb-item"><a href="profile.php">Φοιτητές-To Προφιλ μου</a></li>
                <li class="breadcrumb-item active" aria-current="page">Επεξεργασία Προφιλ</li>


            </ol>
        </nav>
        </div>
        </div>
        <div id="displayerror" class="alert alert-warning alert-dismissible fade show" role="alert" style="display:none">
            <strong>Your Input was exactly the same as the one that already existed</strong> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
        <!--<div style="display:none">
            Your Input was exactly the same as the one that already existed
        </div>-->
        <form id="formtoEdit" method="post">
            <div class="form-row">
                <div class="form-group col-md-3">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                </div>
                <div class="form-group col-md-3">
                <label for="inputPassword4">Password</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                </div>
            </div>
            <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputUniversity">Ιδρυμά</label>
                <input type="text" class="form-control" id="inputUniversity" placeholder="Εθνικό και Καποδιστριακό Πανεπιστήμιο Αθηνών">
            </div>
            </div>
            <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputSchool">Σχολή</label>
                <input type="text" class="form-control" id="inputSchool" placeholder="Πληροφορικής και Τηλεπικοινωνιων">
            </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                <label for="inputAM">Αριθμός Μητρώου</label>
                <input type="text" class="form-control" id="inputAM">
                </div>

                <div class="form-group col-md-2">
                <label for="inputPhone">Κινητό Τηλέφωνο</label>
                <input type="text" class="form-control" id="inputPhone">
                </div>
            </div>
            <div class="form-group">
                <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Θα ήθελα να λαμβάνω ενημερώσεις σχετικά με τις ανακοινώσεις και τα νέα του Εύδοξου
                </label>
                </div>
            </div>
            <a href="profile.php" class="btn btn-danger mr-2">Άκυρο</a>
            <button type="submit" onclick=changeprofile() class="btn btn-success">Ολοκλήρωση</button>
        </form>
    </div>
        <footer class="footer rounded font-small footer-dark pt-2 mt-2" style="background-color:#f5f5dc;">
        <div class="container-fluid text-center text-md-left">
            <div class="row">
            <div class="col-md-6 mt-md-0 mt-3">
                <!-- Content -->
                <h5 class="text-uppercase">Footer Content</h5>
                <p>Here you can use rows and columns here to organize your footer content.</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none ">

            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 ">

                <!-- Links -->
                <h5 class="text-uppercase">Links</h5>

                <ul class="list-unstyled">
                    <li>
                    <a href="#!">Link 1</a>
                    </li>
                    <li>
                    <a href="#!">Link 2</a>
                    </li>
                </ul>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-3 mb-md-0 ">

                <!-- Links -->
                <h5 class="text-uppercase">Links</h5>

                <ul class="list-unstyled">
                    <li>
                    <a href="#!">Link 1</a>
                    </li>
                    <li>
                    <a href="#!">Link 2</a>
                    </li>
                </ul>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->
        <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
        </div>    

    </footer>
</body>
<script>
function logout(){
  
  /*<?php
    unset($_SESSION['username']);
    session_destroy();
   ?>*/
   window.location.href = "http://localhost/sdi1500061_sdi1500117/logout.php";
}
</script>

<script>
function changeprofile(){
        request = $.ajax({
            url: "changeProfile.php",
            type: "post",
            data: {action: 'changeProfile',
                   email: document.getElementById('inputEmail4').value,
                   password: document.getElementById('inputPassword4').value,
                   university: document.getElementById('inputUniversity').value,    //ta exw balei anapoda gt anapoda einai sth bash kai alliws edw
                   school: document.getElementById('inputSchool').value,
                   AM:  document.getElementById('inputAM').value,
                   Phone: document.getElementById('inputPhone').value}
        })
        request.done(function(response){
            console.log(response);
            if(response==1){
                window.location.href = "http://localhost/sdi1500061_sdi1500117/profile.php";
                
            }
            else{
                document.getElementById('displayerror').style.display = "block";

            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend");
        })
     
    }
</script>


<script>
    $("#formtoEdit").submit(function(e) {
        e.preventDefault();
    });
</script>

</html>
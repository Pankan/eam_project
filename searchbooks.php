<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Eudoxus-Δήλωση Συγγραμάτων</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/bootstrap-grid.css" type="text/css">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/bootstrap.bundle.min.js"></script>
</head>


<body>
    <div class="container-fluid">
        <div class="row mt-1">
        <div class="col-4"></div>
        <div class="col-6 home-image">
            <img src="evdoksos.png" alt="placeholder" style="border:1px solid black;width:400px;height:150px;" class="rounded">  
        </div>
        <div class="col-2"></div>    
        </div>
        <div class="row mt-2">
        <div class="col-12">
        <nav class="navbar rounded sticky-top navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="index.php">Εύδοξος</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Αρχική <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Φοιτητές
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="profile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dilosi.php">Δήλωση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Ανταλλαγή Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="istorikodilwsewn.php">Ιστορικό Δηλώσεων</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Εκδότες
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="ekdotisprofile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="ekdothsBiblia.php">Διαχείρηση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Κοστολόγηση</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Γραμματεία
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Placeholder</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Placeholder</a>
                </div>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Νεα-Ανακοινώσεις</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="searchbooks.php">Αναζήτηση Βιβλίων</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="help.php">Βοήθεια</a>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Username"] ?>
                    
                  </a>
                  
                <?php else: ?>
                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Ekdoths"] ?>
                    </a>
                    
                  <?php else: ?>
                    <li class="nav-item"><a class="nav-link" href="./register.php"><i class="fas fa-user-plus mr-1"></i> Εγγραφή</a></li>

                  <?php endif ?>

                <?php endif ?>
                <li class="nav-item">
                <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="logout.php"> 
                    
                    <?php echo "Αποσύνδεση" ?>
                    
                  </a>
                <?php else: ?>

                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="logout.php"> 
                    
                      <?php echo "Αποσύνδεση" ?>
                    </a>
                  <?php else: ?>
                    <a class="nav-link" href="login.php"><i class="fas fa-sign-in-alt mr-1"></i> 
                    Σύνδεση
                    </a>
                  <?php endif ?>

                <?php endif ?>
                
              </li>
            </ul>  
          </div>
        </nav>
        </div>
        </div>
        <!--Breadcrunmb here-->
        <div class="row">
        <div class="col-md-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mt-0 mb-2 bg-light">
                <!--All previous pages here -->
                <li class="breadcrumb-item"><a href="index.php">Αρχική</a></li>
                <!--Current Page-->
                <li class="breadcrumb-item active" aria-current="page">Αναζήτηση Βιβλίων</li>


            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-2"></div>
    <div class="col-2">Γράψτε Εδώ τίτλο βιβλίου ή συγγραφέα ή εκδοτικο οίκο ή ISBN:</div>
    <div class="col-8">
        <div class="input-group col-md-4">
                    <input class="form-control py-2 border-right-0 border" type="search"  id="inputBook">
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary border-left-0 border" onclick=clickSearch() type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
        </div>
        </div>
</div>
<div  class="row"  >
    <div class="col-2"></div>
    <div class="col-2 mt-4" id="displaynumber" style="display:none">
    <div class="alert alert-dark" role="alert">
        Βρέθηκαν <strong id="number"></strong> Αποτελέσματα
    </div>
        
    </div>
</div>
<div class="container-fluid" id="results">
<!-- here goes code from php-->
</div>
<div  class="row mt-5">
    <div class="col-2"></div>
    <div class="col-2">
      <a class="btn btn-danger" href="index.php"> < Πίσω</a>
    </div>
</div>
<footer class="footer font-small footer-dark pt-2 mt-5" id="footer" style="background-color:#f5f5dc;">
      <div class="container-fluid text-center text-md-left">
        <div class="row">
          <div class="col-md-6 mt-md-0 mt-3">
            <!-- Content -->
            <h5 class="text-uppercase">Footer Content</h5>
            <p>Here you can use rows and columns here to organize your footer content.</p>

          </div>
          <!-- Grid column -->

          <hr class="clearfix w-100 d-md-none ">

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 ">

              <!-- Links -->
              <h5 class="text-uppercase">Links</h5>

              <ul class="list-unstyled">
                <li>
                  <a href="#!">Link 1</a>
                </li>
                <li>
                  <a href="#!">Link 2</a>
                </li>
              </ul>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 ">

              <!-- Links -->
              <h5 class="text-uppercase">Links</h5>

              <ul class="list-unstyled">
                <li>
                  <a href="#!">Link 1</a>
                </li>
                <li>
                  <a href="#!">Link 2</a>
                </li>
              </ul>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>
      <!-- Footer Links -->
      <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
      </div>
    </footer>
<script>
function clickSearch(){
        //take only number of results
        request = $.ajax({
            url: "search.php",
            type: "post",
            data: {action: 'returnnumberofresults',
                   input: document.getElementById('inputBook').value}
        })
        request.done(function(response){
            console.log(response);
            if(response!=-1){
                document.getElementById('number').innerHTML = response;
                document.getElementById('displaynumber').style.display = "block";

                
            }
            else{
                console.log("magka sou lew");
            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend");
        })



        //take all results 
        request = $.ajax({
            url: "search.php",
            type: "post",
            data: {action: 'searchbook',
                   input: document.getElementById('inputBook').value}
        })
        request.done(function(response){
           console.log(document.getElementById('inputBook').value);
            console.log(response);
            if(response!=-1){
                

                    document.getElementById('results').innerHTML = response;
                
            }
            else{
                console.log("magka sou lew");
            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend");
        })
     
    }

</script>

<script>
function logout(){
  
  /*<?php
    unset($_SESSION['username']);
    session_destroy();
   ?>*/
   window.location.href = "http://localhost/sdi1500061_sdi1500117/logout.php";
}
</script>
</body>
</html>
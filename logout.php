<?php 
session_start();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/login.css" />
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    
    <script src="./js/jquery.js"></script>
    
</head>

<div class="row" style="margin-top:20%;">
  <div class="col-4"></div>
  <div class="col-5">
    <div class="alert alert-success " role="alert">
      Επιτυχής αποσύνδεση!
      <a href="index.php"> Επιστροφή στην αρχική σελίδα</a>
    </div>
  </div>
 
<?php 
  unset($_COOKIE["Username"]);
  unset($_COOKIE["Id"]);
  unset($_COOKIE["Ekdoths"]);
  unset($_COOKIE["EkdothsId"]);
  setcookie('Username', null, -1, '/');
  setcookie('Id', null, -1, '/');
  setcookie('Ekdoths', null, -1, '/');
  setcookie('EkdothsId', null, -1, '/');
?>
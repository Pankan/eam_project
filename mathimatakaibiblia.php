<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Eudoxus-Δήλωση Συγγραμάτων</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/bootstrap-grid.css" type="text/css">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="./js/popper.min.js"></script>
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row mt-1">
            <div class="col-4"></div>
            <div class="col-6 home-image">
                <img src="images/evdoksos.png" alt="placeholder" style="border:1px solid black;width:400px;height:150px;" class="rounded">  
            </div>
            <div class="col-2"></div>    
        </div>
        <div class="row mt-2">
            <div class="col-12">
            <nav class="navbar rounded sticky-top navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="index.php">Εύδοξος</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Αρχική <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Φοιτητές
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="profile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dilosi.php">Δήλωση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Ανταλλαγή Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="istorikodilwsewn.php">Ιστορικό Δηλώσεων</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Εκδότες
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="ekdotisprofile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="ekdothsBiblia.php">Διαχείρηση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Κοστολόγηση</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Γραμματεία
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Placeholder</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Placeholder</a>
                </div>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Νεα-Ανακοινώσεις</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="searchbooks.php">Αναζήτηση Βιβλίων</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="help.php">Βοήθεια</a>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Username"] ?>
                    
                  </a>
                  
                <?php else: ?>
                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Ekdoths"] ?>
                    </a>
                    
                  <?php else: ?>
                    <li class="nav-item"><a class="nav-link" href="./register.php"><i class="fas fa-user-plus mr-1"></i> Εγγραφή</a></li>

                  <?php endif ?>

                <?php endif ?>
                <li class="nav-item">
                <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="logout.php"> 
                    
                    <?php echo "Αποσύνδεση" ?>
                    
                  </a>
                <?php else: ?>

                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="logout.php"> 
                    
                      <?php echo "Αποσύνδεση" ?>
                    </a>
                  <?php else: ?>
                    <a class="nav-link" href="login.php"><i class="fas fa-sign-in-alt mr-1"></i> 
                    Σύνδεση
                    </a>
                  <?php endif ?>

                <?php endif ?>
                
              </li>
            </ul>  
          </div>
        </nav>
            </div>
        </div>
        <!--Breadcrunmb here-->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mt-0 mb-2 bg-white">
                <!--All previous pages here -->
                <li class="breadcrumb-item"><a href="index.php">Αρχική</a></li>
                <li class="breadcrumb-item"><a href="dilosi.php">Φοιτητές-Δήλωση Συγγραμάτων</a></li>
                <!--current page-->
                <li class="breadcrumb-item active" aria-current="page">Μαθηματα Προς Δήλωση</li>


            </ol>
        </nav>
        <!--Breadcrunmb end here--> 
        <?php if (isset($_COOKIE['Username'])) : ?>

        <?php else: ?>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="alert alert-warning" role="alert">
                        Δεν έχετε συνδεθεί. Οποιαδήποτε αλλαγή θα χάθει με την επικυρωση δήλωσης και θα οδηγηθείτε στην σελίδα συνδεσης.
                    </div>
                </div>
            </div>
        <?php endif ?>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8" id ="alertforsucces">

            </div>
        </div>
<div class="row">
    <div class="col-2"> </div>
    <div class="col-9">
        <form class="form-inline">
             <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Επιλέξτε Σχολή</label>
           <select class="custom-select my-1 mr-sm-2" onclick=showCourses(this.value) id="dropdown" placeholder="Επιλέξτε..">
          
            </select>
           

            

            
        </form>
    </div>
</div>

</div>

    <div class="row mt-3">
        <div class="col-2"> </div>
        <div class="col-2 ">
            <div class="list-group" id="list-tab" role="tablist" >
            
            </div>
        </div>
        <div class="col-2 " id="books" style="display:none">

        </div>
        <div class="col-3 ">
            <div class="card" id="cardofDhlwsi" style="width: 18rem;">
                    <strong> Τα Βιβλία που έχετε επιλέξει είναι: </strong>
                    <ul class="list-group list-group-flush" id="listDhlwsis">
                        <li class="list-group-item">Δεν έχετε επιλέξει βιβλίο</li>
                        
                    </ul>

            </div>
        </div>

    </div>

    <div class="row mt-2">
        <div class="col-2"></div>
        <div class="col-2">
            <a class="btn btn-danger" href="dilosi.php"> < Πίσω </a>
        </div>
        <div class="col-2"></div>
        <div class="col-2">
            <button type="button" id="buttontoinsertDhlwsh" class="btn btn-success"<?php if (isset($_COOKIE['Username'])) : ?>onclick="insertDilwsh(5)"<?php else: ?>onclick="needtologin()"<?php endif ?> disabled=true >Επικύρωση δήλωσης</button> <!-- mesa sthn synarthsh bazw to trexon eksasmhno-->
        </div>

    </div>
</div>
</div>
<footer class="footer font-small footer-dark pt-2 mt-2 " style="background-color:#f5f5dc;">
      <div class="container-fluid text-center text-md-left">
        <div class="row">
          <div class="col-md-6 mt-md-0 mt-3">
            <!-- Content -->
            <h5 class="text-uppercase">Footer Content</h5>
            <p>Here you can use rows and columns here to organize your footer content.</p>

          </div>
          <!-- Grid column -->

          <hr class="clearfix w-100 d-md-none ">

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 ">

              <!-- Links -->
              <h5 class="text-uppercase">Links</h5>

              <ul class="list-unstyled">
                <li>
                  <a href="#!">Link 1</a>
                </li>
                <li>
                  <a href="#!">Link 2</a>
                </li>
              </ul>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 ">

              <!-- Links -->
              <h5 class="text-uppercase">Links</h5>

              <ul class="list-unstyled">
                <li>
                  <a href="#!">Link 1</a>
                </li>
                <li>
                  <a href="#!">Link 2</a>
                </li>
              </ul>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>
      <!-- Footer Links -->
      <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
      </div>
    </footer>

<script>
    function needtologin(){
        window.location.href = "http://localhost/sdi1500061_sdi1500117/login.php";
    }
</script>

<script>
function insert(trexonEksamhno){

        console.log(trexonEksamhno);
        request = $.ajax({
            url: "bashgiadilosi.php",
            type: "post",
            data: {action: 'insertDilwsh',
                userid: '<?php  echo $_COOKIE["Id"];?>',
                ids: id,
                titloi: titloi,
                sizeofarray: titloi.length,
                trexoneksamhno: trexonEksamhno}
        })
        request.done(function(response){
            console.log(response);
            if(response!=-1){
                document.getElementById('alertforsucces').innerHTML = response;
                
            }
            else{

            }
        })
        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
    }
</script>

<script>
    function insertDilwsh(trexonEksamhno){

        request = $.ajax({
            url: "bashgiadilosi.php",
            type: "post",
            data: {action: 'insertDilwsh',
                userid: '<?php  echo $_COOKIE["Id"];?>',
                ids: id,
                titloi: titloi,
                sizeofarray: titloi.length,
                trexoneksamhno: trexonEksamhno}
        })
        request.done(function(response){
            if(response!=-1){
                if(response == 2){
                   //insert(trexonEksamhno);
                }
                else{
                    document.getElementById('alertforsucces').innerHTML = response;

                }
              
            }
            else{
                /*document.getElementById('listDhlwsis').innerHTML = '<li class="list-group-item">Δεν έχετε επιλέξει κανένα βιβλίο</li>';
                document.getElementById('cardofDhlwsi').style.display = "block";*/
            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
    }
</script>

<script>
    function showDilwsh(trexonEksamhno){

        request = $.ajax({
            url: "bashgiadilosi.php",
            type: "post",
            data: {action: 'showDilwsh',
                ids: id,
                titloi: titloi,
                sizeofarray: titloi.length,
                trexoneksamhno: trexonEksamhno}
        })
        request.done(function(response){
            if(response!=-1){
                document.getElementById('listDhlwsis').innerHTML = response;
                document.getElementById('cardofDhlwsi').style.display = "block";
                document.getElementById('buttontoinsertDhlwsh').style.display = "block"
            }
            else{
                document.getElementById('listDhlwsis').innerHTML = '<li class="list-group-item">Δεν έχετε επιλέξει κανένα βιβλίο</li>';
                document.getElementById('cardofDhlwsi').style.display = "block";

            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
    }
</script>


<script>
    $(document).ready(function(){
        request = $.ajax({
            url: "bashgiadilosi.php",
            type: "post",
            data: {action: 'epiloghsxolhs'}
        })
        request.done(function(response){
            if(response!=-1){
                document.getElementById('dropdown').innerHTML  = response;
                
            }
            else{
                document.getElementById('displayerror').style.display = "block";

            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
        
    })

</script>

<script>
    function showCourses($School){
        request = $.ajax({
            url: "bashgiadilosi.php",
            type: "post",
            data: {action: 'epiloghmathimatos',
                    school: $School}
        })
        request.done(function(response){
            if(response!=-1){
                document.getElementById('list-tab').innerHTML = response;
                document.getElementById('list-tab').style.display = "block";
                document.getElementById('books').style.display = "none";
            }
            else{
                document.getElementById('list-tab').style.display = "none";

            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
        
    }

    </script>

    <script>
    var idbookandidmathima=[];
    var idmathima=[];
    var id=[];
    var titloi=[];
    var counter = 0;
    </script>

<script>

    function showBooksOfCourses($Course){

        request = $.ajax({
            url: "bashgiadilosi.php",
            type: "post",
            data: {action: 'showbooks',
                   idmathima: $Course}
        })
        request.done(function(response){
            if(response!=-1){
                document.getElementById('books').innerHTML = response;
                document.getElementById('books').style.display = "block";
                counter=0;
                idmathima.push($Course);
                for( var i = 0; i < id.length; i++){ 
                    if(idbookandidmathima[i][1] == $Course){
                        
                        document.getElementById(id[i]).checked = true;
                    }
                }
               
            }
            else{
                document.getElementById('books').style.display = "none";

            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
        
    }

</script>


<script>
 
   function insertBiblio($idBiblio , $Titlos){
        var flag=0;
        for( var i = 0; i < id.length; i++){ 
            if ( id[i] === $idBiblio) {
                counter = 0
                flag=1;
            }
        }
        if(flag === 0){
            counter=1;
        }
       
       if(counter==1){
            for( var i = 0; i < idmathima.length; i++){ 
                var temp = idmathima[i];
            }
            idbookandidmathima.push([$idBiblio,temp]);

           
            id.push($idBiblio);
            titloi.push($Titlos)
           
                
            
       }
       else{
           counter=0;
           var temp;
           for( var i = 0; i < idmathima.length; i++){ 
                temp = idmathima[i];
            }
           for( var i = 0; i < id.length; i++){ 
                if ( id[i] === $idBiblio) {
                    console.log(idbookandidmathima[i]);
                    idbookandidmathima.splice(i,1);
                    console.log(idbookandidmathima[i]);
                    id.splice(i, 1); 
                }
            }
            for( var i = 0; i < titloi.length; i++){ 
                if ( titloi[i] === $Titlos) {
                    titloi.splice(i, 1); 
                }
            }
            
       }
       request = $.ajax({
            url: "bashgiadilosi.php",
            type: "post",
            data: {action: 'showDilwsh',
                ids: id,
                titloi: titloi,
                sizeofarray: titloi.length,
                trexoneksamhno: 5}
        })
        request.done(function(response){
            if(response!=-1){
                document.getElementById('listDhlwsis').innerHTML = response;
                document.getElementById('cardofDhlwsi').style.display = "block";
                document.getElementById('buttontoinsertDhlwsh').disabled = false;

            }
            else{
                document.getElementById('listDhlwsis').innerHTML = '<li class="list-group-item">Δεν έχετε επιλέξει κανένα βιβλίο</li>';
                document.getElementById('cardofDhlwsi').style.display = "block";
                document.getElementById('buttontoinsertDhlwsh').disabled = true;


            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
   }
</script>

<script>
function logout(){
  

   window.location.href = "http://localhost/sdi1500061_sdi1500117/logout.php";
}
</script>


</body>
</html>
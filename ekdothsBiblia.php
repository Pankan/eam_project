<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/bootstrap-grid.css" type="text/css">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="./css/ekdothsBiblia.css" type="text/css">
</head>
<body>
  <div class="container-fluid">
    <div class="row mt-1">
      <div class="col-4"></div>
      <div class="col-6 home-image">
        <img src="images/evdoksos.png" alt="placeholder" style="border:1px solid black;width:400px;height:150px;" class="rounded">  
      </div>
      <div class="col-2"></div>    
    </div>  
    <div class="row mt-2">
      <div class="col-12">
      <nav class="navbar rounded sticky-top navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="index.php">Εύδοξος</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Αρχική <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Φοιτητές
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="profile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="dilosi.php">Δήλωση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Ανταλλαγή Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="istorikodilwsewn.php">Ιστορικό Δηλώσεων</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Εκδότες
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="ekdotisprofile.php">To Προφιλ μου</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="ekdothsBiblia.php">Διαχείρηση Συγγραμμάτων</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Κοστολόγηση</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Γραμματεία
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Placeholder</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Placeholder</a>
                </div>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Νεα-Ανακοινώσεις</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="searchbooks.php">Αναζήτηση Βιβλίων</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="help.php">Βοήθεια</a>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Username"] ?>
                    
                  </a>
                  
                <?php else: ?>
                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="#"> 
                    
                    <?php echo $_COOKIE["Ekdoths"] ?>
                    </a>
                    
                  <?php else: ?>
                    <li class="nav-item"><a class="nav-link" href="./register.php"><i class="fas fa-user-plus mr-1"></i> Εγγραφή</a></li>

                  <?php endif ?>

                <?php endif ?>
                <li class="nav-item">
                <?php if (isset($_COOKIE["Username"])) : ?>
                  <a class="nav-link" href="logout.php"> 
                    
                    <?php echo "Αποσύνδεση" ?>
                    
                  </a>
                <?php else: ?>

                  <?php if (isset($_COOKIE["Ekdoths"])) : ?>
                    <a class="nav-link" href="logout.php"> 
                    
                      <?php echo "Αποσύνδεση" ?>
                    </a>
                  <?php else: ?>
                    <a class="nav-link" href="login.php"><i class="fas fa-sign-in-alt mr-1"></i> 
                    Σύνδεση
                    </a>
                  <?php endif ?>

                <?php endif ?>
                
              </li>
            </ul>  
          </div>
        </nav>
      </div>
    </div>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb mt-0 mb-2 bg-white">
          <!--All previous pages here -->
          <li class="breadcrumb-item"><a href="index.php">Αρχική</a></li>
          <!--Current Page-->
          <li class="breadcrumb-item active" aria-current="page">Εκδότης-Διαχείρηση Συγγραμάτων</li>

      </ol>
    </nav>
    <div class="row mt-2">
      <div class="col-1">
            <button  id="energh-tab"  class="btn btn-dark" style="width:113%;height:40px" data-target="#energh,#kwdikos,#titlos,#syggrafeas,#prosthiki," href="#energh" onclick="myFunction(this.id)">Βιβλία</button>
            <button  id="shmeia-dianomhs-tab" class="btn btn-dark" style="width:auto" data-target="#shmeia-dianomhs,#bibliagiadianomh"href="#shmeia-dianomhs"  onclick="myFunction(this.id)">Σημεία Διανομής</button>
      </div>
      <div class="col-1"></div>

      <div  id="energh" class="col-7"> 
        <table class="table rounded table-striped" id="listBibliwn" style="width:100%">
          
        
          
        </table> 
        <!--<strong style="padding-left:40%">Ενεργή</strong>-->
                  
      </div> 
      <div  id="shmeia-dianomhs" class="col-2">
       
              
        <form>
          <div class="form-group" id="region">
            <script type= "text/javascript" src = "./js/countries.js"></script>
            Περιοχή:   <select id="country" name ="country" class="form-control "></select> <br><br>
            Δήμος: <select name ="state" id ="state" class="form-control "></select>  <br> <br> <hr/>
            <!-- Select Country:   <select id="country2" name ="country2"></select>-->
          </div>
        </form>
        <div class="card">
          <div class="card-body" id="myTable">
            <table>  
              <tbody id="myTable">
                <tr>
                  <td>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                      <label class="form-check-label" for="defaultCheck1">
                        <p >Χατζηφράγκος </p>
                        <a hidden>Λαμία Μπούκοβο</a>
                      </label>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="form-check" >
                      <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                      <label class="form-check-label" for="defaultCheck1">
                        <p >ΠΛΕΥΣΟΝ </p>
                        <a hidden>Αττική Πειραιάς</a>
                      </label>
                    </div>
                  </td> 
                </tr>
              </tbody>
            </table>
          </div>
        </div>    
      </div> 
      <div class="col-1"></div>
      <div id="bibliagiadianomh">
        <div class="card">
          <h5 class="card-header">Συγγράμματα</h5>
          <div class="card-body">
            <h5 class="card-title">Special title treatment</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
          </div>
        </div>
      </div>
    </div> 

 

    <div class="row">
      <div class="col-7"></div>
      <div class="col-2" >
        <div  id="prosthiki">
          <button class="btn btn-success mt-2" style="margin-top=20%" onclick="document.location.href='kataxwrhsh.php';"> <i class="fas fa-plus mr-2"></i>Προσθήκη Συγγράμματος</button>
        </div>
      </div>  
    </div>
  </div>
  <footer class="footer rounded font-small footer-dark pt-2 mt-2" style="background-color:#f5f5dc;">
        <div class="container-fluid text-center text-md-left">
            <div class="row">
            <div class="col-md-6 mt-md-0 mt-3">
                <!-- Content -->
                <h5 class="text-uppercase">Footer Content</h5>
                <p>Here you can use rows and columns here to organize your footer content.</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none ">

            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 ">

                <!-- Links -->
                <h5 class="text-uppercase">Links</h5>

                <ul class="list-unstyled">
                    <li>
                    <a href="#!">Link 1</a>
                    </li>
                    <li>
                    <a href="#!">Link 2</a>
                    </li>
                </ul>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-3 mb-md-0 ">

                <!-- Links -->
                <h5 class="text-uppercase">Links</h5>

                <ul class="list-unstyled">
                    <li>
                    <a href="#!">Link 1</a>
                    </li>
                    <li>
                    <a href="#!">Link 2</a>
                    </li>
                </ul>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->
        <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
        </div>    

    </footer>

    <script>
function logout(){
 
   window.location.href = "http://localhost/sdi1500061_sdi1500117/logout.php";
}
</script>
        
    <script>
  $(document).ready(function(){
        request = $.ajax({
            url: "insertbiblio.php",
            type: "post",
            data: {action: 'showBiblia',
                    idEkdoth: '<?php  echo $_COOKIE['EkdothsId'];?>'}
        })
        request.done(function(response){
            console.log(response);
            if(response!=-1){
                document.getElementById('listBibliwn').innerHTML  = response;
            }
            else{
            }
        })

        request.fail(function(jqXHR, textStatus, errorThrown){
            console.error("Error occured my friend")
        })
        
    })

</script>

    <script>
$(document).ready(function(){
  $("#state").on("input", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
     
        if (($(this).text().toLowerCase().indexOf(value) > -1)===true) {
          $(this).show();
          $("#myTable input").show();
          $("#myTable p").show();
        } else {
          $(this).hide();
          $("#myTable input").hide();
          $("#myTable p").hide();
        }
        
    });
  });
});
</script> 


  <script language="javascript">
    populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
  
  </script>





    <script> window.onload = function () {
            document.getElementById("energh-tab").disabled=true;
            document.getElementById("shmeia-dianomhs-tab").disabled=false;
            document.getElementById("shmeia-dianomhs").style.display = "none";
            document.getElementById("bibliagiadianomh").style.display = "none";
            document.getElementById("energh").style.display = "block";
        
    }
    </script>

<script>
function myFunction(btnId) {
    if(btnId === "energh-tab"){
      document.getElementById("energh-tab").disabled=true;
      document.getElementById("shmeia-dianomhs-tab").disabled=false;
      document.getElementById("shmeia-dianomhs").style.display = "none";
      document.getElementById("bibliagiadianomh").style.display = "none";
      document.getElementById("energh").style.display = "block";
    
      document.getElementById("prosthiki").style.display ="block"
    }
    else{
      document.getElementById("energh-tab").disabled=false;
      document.getElementById("shmeia-dianomhs-tab").disabled=true;
      document.getElementById("energh").style.display = "none";
      document.getElementById("shmeia-dianomhs").style.display = "block";
      document.getElementById("bibliagiadianomh").style.display = "block";
      
      document.getElementById("prosthiki").style.display ="none"
    }
}
</script>


<script>
  $(document).ready(function(){
    <?php if (isset($_COOKIE["Ekdoths"])) : ?>
    <?php else: ?>
        window.location.href = "http://localhost/sdi1500061_sdi1500117/index.php";
    <?php endif ?>      
        
    
  })
</script>
</body>
</html>
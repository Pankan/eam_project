<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/login.css" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/bootstrap.bundle.min.js"></script>
</head>


<body>
<div class="container-fluid">
    <div class="row" >
        <div class="col-4"></div>
        <div class="col-5 mt-5">
            <div class="card" id="buttonselection">
                <div class="card-body">
                    <div class="alert alert-warning" role="alert">
                        Πατήστε αν θέλετε να εγγραφείτε  ως εκδότης ή ως φοιτητής;
                    </div>
                   

                    <button  class="btn btn-secondary" onclick=registerFoiththsForm()>Φοιτητής</button>
                    <button  class="btn btn-secondary" onclick=registerEkdothsForm()>Εκδότης</button>
                    <br>
                    <br>
                    <a class="btn btn-link mt-2" href="index.php">< Πίσω</a>
                </div>
            </div>
        </div>                      
    </div>

        

    <div class="row mt-5"  >
        <div class="col-3"></div>
        <div class="col-5 card" id="registerfoitiths" style="width: 16rem;display:none">
            <div class="card-body">
                <div class= "row"   >
                    <div class="col-5"></div>
                    <i class="fas fa-user fa-7x ml-4 pb-2"></i>
                <form id="regfoithths">
                </div>
                <div class="form-group row">
                    <label for="inputemail" class="col-sm-2 col-form-label">Email:</label>
                    <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputemail" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputusername" class="col-sm-2 col-form-label">Username:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputusername" placeholder="Username">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputpassword" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputpassword" placeholder="Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">Name:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputname" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputsurname" class="col-sm-2 col-form-label">Surname:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputsurname" placeholder="Surname">
                    </div>
                </div>
               
                <div class="form-group row">
                    <label for="inputam" class="col-sm-2 col-form-label">AM:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputam" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputschool" class="col-sm-2 col-form-label">School:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputschool" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputphone" class="col-sm-2 col-form-label">Phone:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputphone" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputuniversity" class="col-sm-2 col-form-label">University:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputuniversity" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-3"></div>
                    <div class="col-2">
                        <a class="btn btn-danger" href="index.php">Άκυρο</a>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-6">
                        <button  class="btn btn-success"  onclick=clickRegister() >Εγγραφή</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row mt-5" >
        <div class="col-3"></div>
        <div class="col-5 card"  id="registerekdoths" style="width: 16rem;display:none">
            <div class="card-body">
                <div class= "row"   >
                    <div class="col-5"></div>
                    <i class="fas fa-user fa-7x ml-4 pb-2"></i>
                <form id="regekdoths">
                </div>
                <div class="form-group row">
                    <label for="inputemail" class="col-sm-2 col-form-label">Email:</label>
                    <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputemail1" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputusername" class="col-sm-2 col-form-label">Username:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputusername1" placeholder="Username">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputpassword" class="col-sm-2 col-form-label">Password:</label>
                    <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputpassword1" placeholder="Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">Ονομα Εκδωτικού Οίκου:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputname1" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-3"></div>
                    <div class="col-2">
                        <a class="btn btn-danger" href="index.php">Άκυρο</a>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-6">
                    <button  class="btn btn-success" onclick=clickRegisterEkdoths() >Εγγραφή</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
function registerFoiththsForm(){
    document.getElementById('registerfoitiths').style.display= "block";
    document.getElementById('buttonselection').style.display= "none";
}
</script>

<script>
function registerEkdothsForm(){
    console.log("magkas");
    document.getElementById('registerekdoths').style.display= "block";
    document.getElementById('buttonselection').style.display= "none";
}
</script>

<script>
        function clickRegister(){
            request = $.ajax({
                url: "loginandregister.php",
                type: "post",
                data: {action: 'register',
                    email: document.getElementById('inputemail').value,
                    username: document.getElementById('inputusername').value,
                    password: document.getElementById('inputpassword').value,
                    name: document.getElementById('inputname').value,
                    surname: document.getElementById('inputsurname').value,
                    AM: document.getElementById('inputam').value,
                    School: document.getElementById('inputschool').value,
                    Phone: document.getElementById('inputphone').value,
                    University: document.getElementById('inputuniversity').value}
            })
            request.done(function(response){

                console.log(response);
                if(response==1){

                    window.location.href = "http://localhost/sdi1500061_sdi1500117/index.php";
                }
                else{
                    document.getElementById('displayerror').style.display = "block";

                }
            })

            request.fail(function(jqXHR, textStatus, errorThrown){
            
                console.error("Error occured my friend");
            })
        }
    </script>
    <script>
    $("#regfoithths").submit(function(e) {
        e.preventDefault();
    });
</script>

<script>
        function clickRegisterEkdoths(){
            request = $.ajax({
                url: "loginandregister.php",
                type: "post",
                data: {action: 'registerEkdoth',
                    email: document.getElementById('inputemail1').value,
                    username: document.getElementById('inputusername1').value,
                    password: document.getElementById('inputpassword1').value,
                    ekdoseis: document.getElementById('inputname1').value}
            })
            request.done(function(response){

                console.log(response);
                if(response==1){

                    window.location.href = "http://localhost/sdi1500061_sdi1500117/index.php";
                }
                else{
                    document.getElementById('displayerror').style.display = "block";

                }
            })

            request.fail(function(jqXHR, textStatus, errorThrown){
            
                console.error("Error occured my friend");
            })
        }
    </script>
    <script>
    $("#registerekdoths").submit(function(e) {
        e.preventDefault();
    });
</script>
</body>

</html>
